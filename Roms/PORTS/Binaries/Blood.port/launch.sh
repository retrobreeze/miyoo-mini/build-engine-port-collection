#!/bin/sh
echo $0 $*
progdir=`dirname "$0"`
homedir=`dirname "$1"`

# Timer initialisation
cd /mnt/SDCARD/App/PlayActivity
./playActivity "init"

if [ -f "/mnt/SDCARD/Roms/PORTS/Binaries/Blood.port/FILES_HERE/BLOOD.RFF" ]; then

	killall audioserver
	killall audioserver.mod

	export LD_LIBRARY_PATH="/mnt/SDCARD/Roms/PORTS/Binaries/Blood.port/lib:$LD_LIBRARY_PATH"
	cd "/mnt/SDCARD/Roms/PORTS/Binaries/Blood.port/FILES_HERE"
	../nblood &>"/mnt/SDCARD/Roms/PORTS/Binaries/Blood.port/nblood_output.log"

	/mnt/SDCARD/miyoo/app/audioserver &

	# Timer initialisation
	cd /mnt/SDCARD/App/PlayActivity
	./playActivity "Blood"

else
	cd "/mnt/SDCARD/Roms/PORTS/Binaries/missingFile"
	./infoPanel
fi



