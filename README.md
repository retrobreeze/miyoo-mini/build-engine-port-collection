# Build Engine Port Collection

This collection contains Port Collection entries for Blood, NAM, Redneck Rampage, Redneck Rampage Rides Again, Shadow Warrior and World War II GI.

Copy the Roms folder to the root of your SD Card. Then add the game files to the respective `FILES_HERE` folders.

## Files needed:

### Blood

- BLOOD.INI
- BLOOD.RFF
- GUI.RFF
- SOUNDS.RFF
- SURFACE.DAT
- TILES000.ART through TILES017.ART
- VOXEL.DAT
- Optionally .OGG audio files

### NAM

- NAM.GRP
- NAM.RTS
- GAME.CON

### Redneck Rampage

- REDNECK.GRP
- REDNECK.RTS
- Optionally .OGG audio files

### Redneck Rampage Rides Again

- REDNECK.GRP
- REDNECK.RTS

### Shadow Warrior

- SW.GRP

### World War II GI

- WW2GI.GRP
- WW2GI.RTS
- ENHANCE.CON
- GAME.CON
- USER.CON
- XDEFS.CON
- XUSER.CON
